#***************************************************************************
#* SPDX-FileCopyrightText: 2024 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2024 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
ADD_SUBDIRECTORY(skg_advice)
ADD_SUBDIRECTORY(skg_bookmark)
ADD_SUBDIRECTORY(skg_dashboard)
ADD_SUBDIRECTORY(skg_debug)
ADD_SUBDIRECTORY(skg_delete)
ADD_SUBDIRECTORY(skg_file)
ADD_SUBDIRECTORY(skg_highlight)
ADD_SUBDIRECTORY(skg_print)
ADD_SUBDIRECTORY(skg_properties)
ADD_SUBDIRECTORY(skg_selectall)
ADD_SUBDIRECTORY(skg_undoredo)
ADD_SUBDIRECTORY(skg_monthly)
ADD_SUBDIRECTORY(skg_statistic)
