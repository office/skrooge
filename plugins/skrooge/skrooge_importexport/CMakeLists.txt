#***************************************************************************
#* SPDX-FileCopyrightText: 2024 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2024 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE PLUGIN_IMPORTEXPORT ::..")

PROJECT(plugin_importexport)

FIND_PACKAGE(KF${QT_MAJOR_VERSION} ${KF_MIN_VERSION} REQUIRED COMPONENTS 
  GuiAddons             # Tier 1
)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

SET(skrooge_importexport_SRCS
skgimportexportplugin.cpp)

ki18n_wrap_ui(skrooge_importexport_SRCS skgimportexportpluginwidget_pref.ui)
kconfig_add_kcfg_files(skrooge_importexport_SRCS skgimportexport_settings.kcfgc )

KCOREADDONS_ADD_PLUGIN(skrooge_importexport SOURCES ${skrooge_importexport_SRCS} INSTALL_NAMESPACE "skg_gui")
IF(QT_MAJOR_VERSION STREQUAL "5")
  TARGET_LINK_LIBRARIES(skrooge_importexport KF${QT_MAJOR_VERSION}::Parts KF${QT_MAJOR_VERSION}::KIOCore KF${QT_MAJOR_VERSION}::KIOFileWidgets KF${QT_MAJOR_VERSION}::KIOWidgets skgbasemodeler skgbasegui skgbankmodeler)
ELSE()
  TARGET_LINK_LIBRARIES(skrooge_importexport KF${QT_MAJOR_VERSION}::GuiAddons KF${QT_MAJOR_VERSION}::Parts KF${QT_MAJOR_VERSION}::KIOCore KF${QT_MAJOR_VERSION}::KIOFileWidgets KF${QT_MAJOR_VERSION}::KIOWidgets skgbasemodeler skgbasegui skgbankmodeler)
ENDIF()
########### install files ###############
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skrooge_importexport.rc  DESTINATION  ${KDE_INSTALL_KXMLGUIDIR}/skrooge_importexport )
INSTALL(FILES ${PROJECT_SOURCE_DIR}/skgimportexport_settings.kcfg  DESTINATION  ${KDE_INSTALL_KCFGDIR} )
