<!--
/***************************************************************************
 * SPDX-FileCopyrightText: 2024 S. MANKOWSKI stephane@mankowski.fr
 * SPDX-FileCopyrightText: 2024 G. DE BURE support@mankowski.fr
 * SPDX-License-Identifier: GPL-3.0-or-later
 ***************************************************************************/
-->
<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="content-type" content="text/html;charset=utf-8" />
    <meta http-equiv="Content-Style-Type" content="text/css" />
    <script src="https://code.jquery.com/jquery.js"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/highcharts-3d.js"></script>        
    <style type="text/css">
      body
      {
      background-color: #FFFFFF;
      font-size : small;
      font-family : {{ font_family }};      
      }

      h1
      {
      text-decoration: underline;
      color: #000000;
      }

      h2
      {
      text-decoration: underline;
      color: #999999;
      }

      .table
      {
      border: thin solid #000000;
      border-collapse: collapse;
      background-color: #000000;
      }

      .tabletitle
      {
      background-color: #000000;
      color : #FFFFFF;
      font-weight : bold;
      font-size : normal;
      }

      .tabletotal
      {
      background-color: #999999;
      color : #FFFFFF;
      font-weight : bold;
      }

      tr
      {
      background-color: #FFFFFF;
      padding: 2px;
      }

      td
      {
      padding: 2px;
      white-space: nowrap;
      }
    </style>    
  </head>
  <body>
    <table>
      <tr>    
	<td>
	  <img src="{{ logo_black }}" height="128" width="128"/>
	</td>
	<td align="left">
	  <h1>{{ title_main }}</h1>
	  <small>Date: {{ current_date }}</small><br/>
	  <small>File name: {{ document.fileName }}</small><br/>
	</td>
      </tr>
    </table>
    <h2>{{ title_personal_finance_score }}</h2>
    {% include "default/personal_finance_score.html" %}
    
    <h2>{{ document|display:"f_CURRENTAMOUNT_INCOME" }} &amp; {{ document|display:"f_CURRENTAMOUNT_EXPENSE" }} *</h2>
    <table>
      <tr>
	<td valign="top">   
	  {% include "default/income_vs_expenditure_table.html" %}
	</td>
	<td align="top">
	    <div id="chart4" style="height: 300px; width: 600px"></div>
	</td>
      </tr>
    </table>  	
	
    <table>
      <tr>
	<td valign="top">                         
    <h2>{{ title_main_categories }}</h2>
    <table>
      <tr>
	<td align="center">
	  {% include "default/categories_previous_period_table.html" %}
	   <div id="chart1" style="height: 300px; width: 300px"></div>
	</td>
	<td align="center">
	  {% include "default/categories_period_table.html" %}
	  <div id="chart2" style="height: 300px; width: 300px"></div>
	</tr>
    </table>
    </td>
    <td valign="top">
    <h2>{{ title_variations }}</h2>
      {% for item in report.categories_variations %}
      <li> {{ item|safe }}</li>
      {% endfor %}
      </td>
      </tr>
    </table>    
    
    <table>
      <tr>
	<td valign="top">         
	    <h2>{{ title_account }} *</h2>
	    {% include "default/account_table.html" %}
      <div id="chart5" style="height: 300px; width: 400px"></div>     
	</td>
	<td valign="top">
	    <h2>{{ title_budget }}</h2>
	    {% include "default/budget_table.html" %}	
	</td>
      </tr>
    </table>      
    
    <table>
      <tr>
	<td valign="top">         
	    <h2>{{ title_unit }} *</h2>
	    {% include "default/unit_table.html" %}
	</td>
	<td valign="top">
    <h2>{{ title_portfolio }} *</h2>
     {% include "default/portfolio.html" %}      
     <div id="chart3" style="height: 300px; width: 400px"></div>
	</td>
      </tr>
    </table>    
    </br>
    <small>{{ current_date }} - {{ document.fileName }}</small>  
    <p><small>* {{ msg_amount_unit_date }}</small></p>    
<script language="javascript">
    var item = document.getElementById("table_unit");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";
    
    var item = document.getElementById("table_portfolio");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";
    
    var item = document.getElementById("table_income_vs_expenditure");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";
    
    var item = document.getElementById("table_categories_previous_period");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";
    
    var item = document.getElementById("table_categories_period");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";

    var item = document.getElementById("table_budget");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";

    var item = document.getElementById("table_bank");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";    
    
    var item = document.getElementById("table_account");
    if(item != null) item.className= "table table-bordered table-striped table-condensed";       
    
    $('#chart1').highcharts({
        accessibility: {
            enabled: false
        },
        chart: {
            type: 'pie',
            backgroundColor: 'rgba(0,0,0,0)',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,          
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                },
                cursor: 'pointer',
                point: {
                    events: {
                        click: function () {
                            window.open(this.options.url);
                        }
                    }
                }                
            }        
        },
        series: [{
            type: 'pie',
            name: '{{ title_main_categories }}',
            data: [
	    {% for item in report.categories_previous_period %}
		{% if forloop.first %}
		{% else %}    
			{name: '{{ forloop.counter0 }}', 
			y: {{ item.2 }}, 
			url: 'skg://Skrooge_operation_plugin/SKGOPERATION_CONSOLIDATED_DEFAULT_PARAMETERS/?operationTable=v_suboperation_consolidated&operationWhereClause={{ item.3|encode }}&title={{ item.1|encode }}/{{ report.categories_period.0.2 }}&title_icon=applications-all-symbolic'}
			{% if forloop.last %}{% else %},{% endif %}
		{% endif %}
            {% endfor %}	    
            ]
        }]
    });

    $('#chart2').highcharts({
        accessibility: {
            enabled: false
        },        
        chart: {
            type: 'pie',
            backgroundColor: 'rgba(0,0,0,0)',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,          
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }      
        },
        series: [{
            type: 'pie',
            name: '{{ title_main_categories }}',
            data: [
	    {% for item in report.categories_period %}
		{% if forloop.first %}
		{% else %}    
			{name: '{{ forloop.counter0 }}', 
			y: {{ item.2 }}, 
			url: 'skg://Skrooge_operation_plugin/SKGOPERATION_CONSOLIDATED_DEFAULT_PARAMETERS/?operationTable=v_suboperation_consolidated&operationWhereClause={{ item.3|encode }}&title={{ item.1|encode }}/{{ report.categories_period.0.2 }}&title_icon=applications-all-symbolic'}
			{% if forloop.last %}{% else %},{% endif %}
		{% endif %}
            {% endfor %}	    
            ]
        }]
    });

    $('#chart3').highcharts({
        accessibility: {
            enabled: false
        },        
        chart: {
            type: 'pie',
            backgroundColor: 'rgba(0,0,0,0)',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,          
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }      
        },
        series: [{
            type: 'pie',
            name: '',
            data: [
            {% for item in report.portfolio %}
	      {% if forloop.first %}
		{% else %}
		['{{ item.0 }}', {{ item.5 }}]
		{% if forloop.last %}{% else %},{% endif %}
	      {% endif %}
            {% endfor %}
            ]
        }]
    });

    $('#chart4').highcharts({
        accessibility: {
            enabled: false
        },        
        chart: {
            type: 'column',
            backgroundColor: 'rgba(0,0,0,0)',
            options3d: {
                enabled: true,
                alpha: 15,
                beta: 15,
                viewDistance: 25,
                depth: 40
            }
        },
        title: {
            text: ''
        },
        xAxis: {
            categories: ['{{ report.previous_period }}', '{{ report.period }}']
        },
        yAxis: {
            allowDecimals: true,
            min: 0,
            title: {
                text: ''
            }
        },
	    colors: ['#{{ color_negativetext }}', '#{{ color_positivetext }}'],
        tooltip: {
            pointFormat: '<span style="color:{series.color}">\u25CF</span>{point.y}'
        },
        series: [{
            name: '{{ document|display:"f_CURRENTAMOUNT_EXPENSE" }}',
            data: [{{ report.income_vs_expenditure.2.2 }}, {{ report.income_vs_expenditure.2.3 }}]
        }, {
            name: '{{ document|display:"f_CURRENTAMOUNT_INCOME" }}',
            data: [{{ report.income_vs_expenditure.1.2 }}, {{ report.income_vs_expenditure.1.3 }}]
        }
        ]
    });

    $('#chart5').highcharts({
        accessibility: {
            enabled: false
        },        
        chart: {
            type: 'pie',
            backgroundColor: 'rgba(0,0,0,0)',
            options3d: {
                enabled: true,
                alpha: 45,
                beta: 0
            }
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: false,
                depth: 35,
                dataLabels: {
                    enabled: true,
                    format: '{point.name}'
                }
            }
        },
        series: [{
            type: 'pie',
            name: '',
            data: [
            {% for item in report.bank_table %}
	      {% if forloop.first %}
		{% else %}
		{% if forloop.last %}
		  [null, null]
		{% else %}
		  ['{{ item.1 }}', {{ item.6 }}],
		{% endif %}
	      {% endif %}
            {% endfor %}
            ]
        }]
    });
</script>         
  </body>
</html>
