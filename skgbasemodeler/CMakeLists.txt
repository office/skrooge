#***************************************************************************
#* SPDX-FileCopyrightText: 2024 S. MANKOWSKI stephane@mankowski.fr
#* SPDX-FileCopyrightText: 2024 G. DE BURE support@mankowski.fr
#* SPDX-License-Identifier: GPL-3.0-or-later
#***************************************************************************
MESSAGE( STATUS "..:: CMAKE SKGBASEMODELER ::..")

PROJECT(SKGBASEMODELER)

LINK_DIRECTORIES (${LIBRARY_OUTPUT_PATH})

INCLUDE_DIRECTORIES(${SQLCIPHER_INCLUDE_DIRS})

SET(skgbasemodeler_SRCS
   skgobjectbase.cpp
   skgnamedobject.cpp
   skgnodeobject.cpp
   skgpropertyobject.cpp
   skgdocument.cpp
   skgdocumentprivate.cpp
   skgtransactionmng.cpp
   skgservices.cpp
   skgerror.cpp
   skgtraces.cpp
   skgadvice.cpp
   skgreport.cpp
   skgtreemap.cpp
 )

#build a shared library
ADD_LIBRARY(skgbasemodeler SHARED ${skgbasemodeler_SRCS})

#need to link to some other libraries ? just add them here
SET_TARGET_PROPERTIES(skgbasemodeler PROPERTIES VERSION ${SKG_VERSION} SOVERSION ${SOVERSION} )

TARGET_LINK_LIBRARIES(skgbasemodeler PUBLIC KF${QT_MAJOR_VERSION}::I18n KF${QT_MAJOR_VERSION}::ConfigWidgets KF${QT_MAJOR_VERSION}::CoreAddons
    Qt${QT_MAJOR_VERSION}::Core Qt${QT_MAJOR_VERSION}::Gui Qt${QT_MAJOR_VERSION}::Sql Qt${QT_MAJOR_VERSION}::Qml Qt${QT_MAJOR_VERSION}::Xml Qt${QT_MAJOR_VERSION}::DBus Qt${QT_MAJOR_VERSION}::Concurrent
    KF${QT_MAJOR_VERSION}::KIOCore ${SQLCIPHER_LIBRARIES} KF${QT_MAJOR_VERSION}::IconThemes)
IF(QT_MAJOR_VERSION STREQUAL "6")
    TARGET_LINK_LIBRARIES(skgbasemodeler PUBLIC KF6::TextTemplate)
ELSE()
    TARGET_LINK_LIBRARIES(skgbasemodeler PUBLIC Qt${QT_MAJOR_VERSION}::Script Grantlee5::Templates)
ENDIF()

GENERATE_EXPORT_HEADER(skgbasemodeler BASE_NAME skgbasemodeler)

########### install files ###############
INSTALL(TARGETS skgbasemodeler ${KDE_INSTALL_TARGETS_DEFAULT_ARGS}  LIBRARY NAMELINK_SKIP )
